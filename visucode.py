#define eq ==

                  ##################################################
                  #                                                #
                  #                    VisuCode                    #
                  #                                                #
                  #               By BlueBaritone21                #
                  #                                                #
                  #                LICENSE: GPL v2                 #
                  #                                                #
                  ##################################################

import os
import time
import keyboard

import colorama as cl
from colorama import Back, Fore, Style

reverse = Back.WHITE + Fore.BLACK
bold = Style.BRIGHT
unstyle = Style.RESET_ALL
orange = Fore.YELLOW
orangeBack = Back.YELLOW

fls = False
tru = True

def main():
  clear()
  cl.init()
  print(Back.RESET, end="")
  print(Fore.GREEN + "Visu" + Fore.BLUE + "Code", end="")
  print(Fore.RESET + " Editor\n\nPlease wait...")
  time.sleep(1)
  menu()

categories = ["m","c","l","o","v"]

code    = ["whenRun", "moveInDir", "turnCW"]
values  = [""       , "1"        , "1"     ]


valueCheck = [
              ["b",
               lambda value: value == "t" or value == "f",
               "boolians only (t/f): ", "t"],
              ["i",
               lambda value: value.isdigit()|(value[1:].isdigit()&(value[0]=="-")),
               "intergers only: ", "0"],
              ["s",
               lambda value: value==value,# Should always return True
               "text only: ", "Hello, World!"],
              ["n", 
               lambda value: value.isdigit(),
               "positive whole numbers only: ", "1"],
              ["!",
               lambda value: len(value)==0,
               "This block doesn't accept input. Press enter to return.", ""]
             ]


def repl():
  
  stopped = False
  while( not stopped):
    pass
    time.sleep(0.5)
    if keyboard.is_pressed("s"):
      break


#        opcode       styling               label                 category  type
opcodes = [
        ["moveInDir", Back.BLUE           , "forward _ times"   , "m"     , "i"],
        ["turnCW"   , Back.BLUE           , "turn right _ times", "m"     , "i"],
        ["turnCCW"  , Back.BLUE           , "turn left _ times" , "m"     , "i"],
        ["whenRun"  , Back.LIGHTYELLOW_EX , "when I run_"       , "c"     , "!"],
        ["stop"     , Back.LIGHTYELLOW_EX , "stop all_"         , "c"     , "!"],
        ["say"      , Back.MAGENTA        , "say _ "            , "l"     , "s"],
        ["wait"     , Back.LIGHTYELLOW_EX , "wait _ steps"      , "c"     , "n"],
        ["newVar"   , Back.RED            , "create var _"      , "v"     , "s"],
        ["setVar"   , Back.RED            , "set var _ to a"    , "v"     , "s"],
        ["getVar"   , Back.RED            , "get var _ → a"     , "v"     , "s"],
        ["plus"     , Back.GREEN          , "a + _ → a"         , "o"     , "i"],
        ["cat"      , Back.GREEN          , "join a & _ → a"    , "o"     , "s"],
        ["minus"    , Back.GREEN          , "a - _ → a"         , "o"     , "i"],
        ["times"    , Back.GREEN          , "a * _ → a"         , "o"     , "i"],
        ["div"      , Back.GREEN          , "a / _ → a"         , "o"     , "i"],
        ["mod"      , Back.GREEN          , "a % _ → a"         , "o"     , "i"],
        ["value"    , Back.GREEN          , "_ → a"             , "o"     , "s"],
        ["goto"     , Back.LIGHTYELLOW_EX , "jump to _"         , "c"     , "s"],
        ["lbl"      , Back.LIGHTYELLOW_EX , "label _"           , "c"     , "s"],
        ["ask"      , Back.MAGENTA        , "ask for input → a_", "l"     , "!"],
        ["rem"      , Back.LIGHTYELLOW_EX , "comment _"         , "c"     , "s"],
        ["gotoIf"   , Back.LIGHTYELLOW_EX , "jump to _ if a"    , "c"     , "b"],
]


def insertVal(text, subtext):
  return text[0:text.index("_")] + subtext + text[text.index("_")+1:]


reg = "placeholder"

def isInt(string):
  return string.digit()|((string[1:].digit()&string[0]=="-")if len(string)>0 else fls)
# I use opcodes internally. Rendering converts opcodes to renederable text.

def clear():
  os.system("clear")

def getMetadata(opcode):
  q =  [i[0] for i in opcodes].index(opcode)
  return opcodes[q]

def getDataMeta(type):
  q = [i[0] for i in valueCheck].index(type)
  return valueCheck[q]

def drawCode():
  j=0
  for i in code:
    print(Fore.BLACK+getMetadata(i)[1] +
          insertVal(getMetadata(i)[2],reverse+values[j]+getMetadata(i)[1])+unstyle)
    j+=1

#This is just the UI for playing. The VM is a different Procedure.

def play():
  selection = "w"
  while (selection != "m"):
    clear()
    drawTop("Player")
    for i in range(0, 10):
      for j in range(0, 30):
        print(".", end="")
        j=j
      print(" :" + str(i))
    selection = input("selection: ")
    selection = selection[0] if len(selection) > 0 else "placeholder"
    handleEditorSelection(selection)

#Formatting Utility

def menuForm(item):
  return (bold + item[0] + Style.NORMAL + item[1:])

#Main Menu

def menu():
  clear()
  print(reverse + bold + "MENU", end="")
  for i in range(0,31):
    print("", end=" ")
    i = i
  print("\n"+unstyle+menuForm("code"))
  print(menuForm("play"))
  print(menuForm("help"))
  print(menuForm("quit"))
  selection = input(bold + "\nSelection: " + Style.NORMAL)
  selection = selection[0] if len(selection) > 0 else "menu"

  if (selection == "c"):
    editor()
  elif (selection == "p"):
    play()
  elif (selection == "q"):
    clear()
    print("GOODBYE!")
    time.sleep(0.5)
    clear()
    pass
  elif (selection == "h"):
    help()
  else:
    menu()

# Top Bars

def help():
  clear()
  print(unstyle + bold + Fore.LIGHTYELLOW_EX + "HELP\n")
  print(unstyle + bold+"\nWelcome to VisuCode!\n\nWhat is VisuCode?\n"+unstyle)
  print("\nVisuCode is a visual programming language created ",end="") 
  print("by BlueBaritone21. It is licenssed under the GPL 2.0.",end="")
  print(bold+"\n\nHow do I use it?",end=unstyle)
  print("\n\nWriting and playing code is easy with VisuCode! ",end="")
  print("All the menu items have a"+bold+" highlighted"+unstyle, end="")
  print(" letter. just type the highlighted letter, and press enter!",end="")
  print(" For example, to get to this help screen, type \"h\" ", end="")
  print("and press enter on the main menu.\n\nNow, you're ready to ",end="")
  print("make a simple program!\n\nTo start, in the main menu, ",end="")
  print("select \""+menuForm("code")+"\", then, \""+menuForm("insert")+"\".", end="")
  input("\n\nPress enter to continue.")
  menu()

def drawTop(mode):
  if (mode == "EDITOR"):
    print(reverse + mode, end=": ")
    print(menuForm("menu"), end=" ")
    print(menuForm("insert"), end=" ")
    print(menuForm("value"), end=" ")
    print(menuForm("delete   "))
  else:
    print(reverse, end="PLAYER: " + menuForm("menu "))
    print(Fore.GREEN + menuForm("start ") + Fore.RED +        
    menuForm("end             "))
  print(unstyle)

# Editor Code

def categorySelection():
  clear()
  print(unstyle + bold + "PALLETE" + unstyle)
  print(Fore.LIGHTYELLOW_EX+menuForm("control"))
  print(Fore.BLUE+menuForm("motion"))
  print(Fore.MAGENTA+menuForm("looks"))
  print(Fore.GREEN+menuForm("operators"))
  print(Fore.RED+menuForm("variables") + unstyle)
  pick = input("selection: ")
  if len(pick)>0:
    if (pick[0] in categories):
      return pick[0]
    else: 
      return categorySelection()
  else:
    return categorySelection()

def blockSelection(cat):
  clear()
  blocks = []
  j=0
  for i in opcodes:
    if i[3] == cat:
      substring = insertVal(i[2], reverse+"_"+i[1]) if i[4]!="!" else insertVal(i[2],"")
      print(menuForm(str(j) + ": " + i[1]+substring+unstyle+"\n")) 
      blocks.append(i[0])
      j+=1
  pick = input("selection: ")
  if len(pick)==0 or pick=="b" or pick=="c" or pick=="q":
    clear()
    print(Fore.RED+"canceled"+unstyle)
    time.sleep(0.5)
    return
  if pick.isdigit():
    if int(pick) < len(blocks):
      code.append(blocks[int(pick)])
      type = getMetadata( blocks[int(pick)] )[4]
      data = getDataMeta(type)
      values.append(data[3])
    else:
      blockSelection(cat)
  else:
    blockSelection(cat)

def handleEditorSelection(pick):
  if (pick == "m"):
    menu()
  elif (pick == "i"):
    clear()
    blockSelection(categorySelection())
  elif (pick == "d") and len(code)>0:
    code.pop()
    values.pop()
  elif (pick == "v"):
    handleValueChange(getMetadata(code[-1])[4])
  else:
    pass

def handleValueChange(item):
  text = input(getDataMeta(getMetadata(code[-1])[4])[2])
  while not(getDataMeta(item)[1](text)):
    text = input(getDataMeta(getMetadata(code[-1])[4])[2])
  values[-1] = text


def editor():
  selection = "w"
  while (selection != "m"):
    clear()
    drawTop("EDITOR")
    drawCode()
    selection = input(unstyle + "selection: ")
    selection = selection[0] if len(selection) > 0 else "placeholder"
    handleEditorSelection(selection)

# Yeah!!!!!!!!!!!!!

if __name__ == "__main__":
  main()
